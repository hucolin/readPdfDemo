package com.demo;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Colin on 2017/5/14.
 */
public class ReadPdfDemo {

    public static void main(String[] args) throws Exception {
        // 模板文件路径
        String templatePath = "E:\\acs\\pdf_out\\51box\\12100000000\\2017\\2\\28\\001\\COL28131444199c8rpwr.pdf";
        // 生成的文件路径
        String targetPath = "E:\\acs\\pdf_out\\51box\\12100000000\\2017\\2\\28\\001\\9oij.pdf";
        // 书签名
        String fieldName = "Signature1";
        // 图片路径
        String imagePath = "D:\\aabb.bmp";

        // 读取模板文件
        InputStream input = new FileInputStream(new File(templatePath));
        PdfReader reader = new PdfReader(input);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(targetPath));
        // 提取pdf中的表单
        AcroFields form = stamper.getAcroFields();
        form.addSubstitutionFont(BaseFont.createFont("STSong-Light","UniGB-UCS2-H", BaseFont.NOT_EMBEDDED));

        // 通过域名获取所在页和坐标，左下角为起点
        int pageNo = form.getFieldPositions(fieldName).get(0).page;
        List<AcroFields.FieldPosition> p = form.getFieldPositions(fieldName);
        Rectangle signRect = form.getFieldPositions(fieldName).get(0).position;
        float x = signRect.getLeft();
        float y = signRect.getBottom();

        // 读图片
        Image image = Image.getInstance(imagePath);
        // 获取操作的页面
        PdfContentByte under = stamper.getOverContent(pageNo);
        // 根据域的大小缩放图片
        image.scaleToFit(signRect.getWidth(), signRect.getHeight());
        // 添加图片
        image.setAbsolutePosition(x, y);
        under.addImage(image);

        stamper.close();
        reader.close();
    }


    public static void insertText(PdfStamper ps, AcroFields s)
    {
        List<AcroFields.FieldPosition> list = s.getFieldPositions("Signature1");
        Rectangle rect = list.get(0).position;


        PdfContentByte cb = ps.getOverContent(1);
        PdfPTable table = new PdfPTable(1);
        float tatalWidth = rect.getRight() - rect.getLeft() - 1;
        table.setTotalWidth(tatalWidth);


        PdfPCell cell = new PdfPCell(new Phrase(CreateChunk()));
        cell.setFixedHeight(rect.getTop() - rect.getBottom() - 1);
        cell.setBorderWidth(0);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setLeading(0, (float) 1.1);


        table.addCell(cell);
        table.writeSelectedRows(0, -1, rect.getLeft(), rect.getTop(), cb);
    }

    public static Chunk CreateChunk()
    {
        BaseFont bfChinese;
        Chunk chunk = null;
        try
        {
            bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
            Font fontChinese = new Font(bfChinese, 10 * 4 / 3);
            chunk = new Chunk("张三", fontChinese);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        return chunk;
    }
}
